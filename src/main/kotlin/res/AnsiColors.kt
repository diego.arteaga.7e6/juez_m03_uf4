package jutgeV1.res

object AnsiColors {
    // Text color
    const val RESET = "\u001b[39m"
    const val BLACK = "\u001b[30m"
    const val RED = "\u001b[31m"
    const val GREEN = "\u001b[32m"
    const val YELLOW = "\u001b[33m"
    const val BLUE = "\u001b[34m"
    const val MAGENTA = "\u001b[35m"
    const val CYAN = "\u001b[36m"
    const val LIGHT_GRAY = "\u001b[37m"

    // Background color
    const val BG_RESET = "\u001b[49m"
    const val BG_BLACK = "\u001b[40m"
    const val BG_RED = "\u001b[41m"
    const val BG_GREEN = "\u001b[42m"
    const val BG_YELLOW = "\u001b[43m"
    const val BG_BLUE = "\u001b[44m"
    const val BG_MAGENTA = "\u001b[45m"
    const val BG_CYAN = "\u001b[46m"
    const val BG_LIGHT_GRAY = "\u001b[47m"

    // Extended color
    const val COLOR_255 = "\u001b[38;5;255m"
    const val BG_COLOR_255 = "\u001b[48;5;255m"

    // Reset all
    const val RESET_ALL = "\u001b[0m"

    // Text styles
    const val BOLD = "\u001b[1m"
    const val DIM = "\u001b[2m"
    const val UNDERLINED = "\u001b[4m"
    const val BLINK = "\u001b[5m"
    const val REVERSE = "\u001b[7m"
    const val HIDDEN = "\u001b[8m"

    // Reset text styles
    const val RESET_BOLD = "\u001b[21m"
    const val RESET_DIM = "\u001b[22m"
    const val RESET_UNDERLINED = "\u001b[24m"
    const val RESET_BLINK = "\u001b[25m"
    const val RESET_REVERSE = "\u001b[27m"
    const val RESET_HIDDEN = "\u001b[28m"
}
